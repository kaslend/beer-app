# README #
App must run on php 7.1 or greater  
To setup app you must:  
	Run composer install  
	Change database settings in .env file to yours.  
	to run migrations in console run command: bin/console doctrine:migrations:migrate  
	to import breweries data run: app:import-data  
	to calculate route to visit breweries run:  
	bin/console app:plan-breweries-trip --lat=latitude-coordinates --long=longitute-coordinates --distance=travel-distance(optional default 2000km)  
	to run tests: vendor/bin/phpunit -c ./  