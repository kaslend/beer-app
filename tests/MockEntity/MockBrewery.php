<?php
declare(strict_types=1);

namespace Tests\MockEntity;

use App\Entity\Brewery;

/**
 * Class MockBrewery
 * @package Tests\MockEntity
 */
class MockBrewery extends Brewery
{
    /**
     * @param mixed $id
     * @return MockBrewery
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
}
