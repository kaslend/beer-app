<?php
declare(strict_types=1);

namespace Tests\RoutePlanner\Util;

use App\Model\LocationPoint;
use App\RoutePlanner\Util\BreweriesTripPlannerUtil;
use App\Util\DistanceCalculationUtil;
use Tests\BaseUnitTestCase;
use Tests\DataCreatorTrait\MockBreweryCreatorTrait;

/**
 * Class BreweriesTripPlannerTest
 * @package Tests\DataProcessor
 */
class BreweryTripPlannerUtilTest extends BaseUnitTestCase
{
    use MockBreweryCreatorTrait;

    /**
     * @var DistanceCalculationUtil|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $distanceCalculationUtil;

    /**
     * @var BreweriesTripPlannerUtil
     */
    protected $breweriesTripPlannerUtil;

    public function setUp()
    {
        $this->distanceCalculationUtil = $this->createMock(DistanceCalculationUtil::class);
        $this->breweriesTripPlannerUtil = new BreweriesTripPlannerUtil($this->distanceCalculationUtil);
    }

    public function testReturnsCorrectDistanceAndBrewery()
    {
        $brewery1 = $this->createBrewery(1, 'brewery-1', 100, 200);
        $brewery2 = $this->createBrewery(2, 'brewery-2', 200, 400);

        $this
            ->distanceCalculationUtil
            ->expects($this->exactly(2))
            ->method('getDistanceBetweenPoints')
            ->willReturnOnConsecutiveCalls(168.7, 133.0);

        $result = $this
            ->breweriesTripPlannerUtil
            ->getNearestDestination(new LocationPoint(100, 200), [$brewery1, $brewery2]);

        $this->assertEquals($result->getDistance(), 133.0);
        $this->assertEquals($result->getBrewery(), $brewery2);
    }
}
