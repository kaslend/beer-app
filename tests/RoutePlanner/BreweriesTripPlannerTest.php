<?php
declare(strict_types=1);

namespace Tests\RoutePlanner;

use App\DataProvider\BreweryDataProvider;
use App\Model\Destination;
use App\Model\LocationPoint;
use App\RoutePlanner\BreweriesTripPlanner;
use App\RoutePlanner\Util\BreweriesTripPlannerUtil;
use App\Util\DistanceCalculationUtil;
use Tests\BaseUnitTestCase;
use Tests\DataCreatorTrait\MockBreweryCreatorTrait;

/**
 * Class BreweriesTripPlannerTest
 * @package Tests\DataProcessor
 */
class BreweriesTripPlannerTest extends BaseUnitTestCase
{
    use MockBreweryCreatorTrait;

    /**
     * @var DistanceCalculationUtil|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $distanceCalculationUtil;

    /**
     * @var BreweryDataProvider|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $breweriesDataProvider;

    /**
     * @var BreweriesTripPlannerUtil|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $breweriesTripPlannerUtil;

    /**
     * @var BreweriesTripPlanner
     */
    protected $breweriesTripPlanner;

    public function setUp()
    {
        $this->breweriesDataProvider = $this->createMock(BreweryDataProvider::class);
        $this->distanceCalculationUtil = $this->createMock(DistanceCalculationUtil::class);
        $this->breweriesTripPlannerUtil = $this->createMock(BreweriesTripPlannerUtil::class);

        $this->breweriesTripPlanner = new BreweriesTripPlanner(
            $this->distanceCalculationUtil,
            $this->breweriesDataProvider,
            $this->breweriesTripPlannerUtil
        );
    }

    public function testWithNoBreweriesInAList()
    {
        $this->breweriesDataProvider->expects($this->once())->method('loadBreweriesInRange')->willReturn([]);
        $result = $this->breweriesTripPlanner->planRoute(1000, new LocationPoint(100, -200));
        $this->assertEmpty($result->getDestinations());
    }

    public function testReturnsCorrectRoute()
    {
        $brewery1 = $this->createBrewery(1, 'brewery-1', 100, 200);
        $brewery2 = $this->createBrewery(2, 'brewery-2', 200, 400);
        $brewery3 = $this->createBrewery(3, 'brewery-3', 300, 500);

        $brewery1Destination = new Destination(500.0, $brewery1->getLocationPoint(), $brewery1->getName(), $brewery1);
        $brewery2Destination = new Destination(500.0, $brewery2->getLocationPoint(), $brewery2->getName(), $brewery2);

        $this
            ->breweriesDataProvider
            ->expects($this->once())
            ->method('loadBreweriesInRange')
            ->willReturn([$brewery1, $brewery2, $brewery3]);

        $this->breweriesTripPlannerUtil
            ->expects($this->exactly(2))->method('getNearestDestination')
            ->willReturnOnConsecutiveCalls($brewery1Destination, $brewery2Destination);

        $this
            ->distanceCalculationUtil
            ->expects($this->exactly(2))
            ->method('getDistanceBetweenPoints')
            ->willReturnOnConsecutiveCalls(133.0, 168.7);

        $result = $this->breweriesTripPlanner->planRoute(1000, new LocationPoint(100, -200));

        $this->assertEquals(668.7, $result->getTotalDistance());
        $this->assertCount(2, $result->getDestinations());
    }
}
