<?php
declare(strict_types=1);

namespace Tests\DataCreatorTrait;

use Tests\MockEntity\MockBrewery;

/**
 * Trait MockBreweryCreatorTrait
 * @package Tests\DataCreatorTrait
 */
trait MockBreweryCreatorTrait
{
    /**
     * @param int $id
     * @param string $name
     * @param float $lat
     * @param float $long
     *
     * @return MockBrewery
     */
    protected function createBrewery(int $id, string $name, float $lat, float $long)
    {
        $brewery = new MockBrewery();
        $brewery->setId($id);
        $brewery->setName($name);
        $brewery->setLat($lat);
        $brewery->setLong($long);

        return $brewery;
    }
}
