<?php
declare(strict_types=1);

namespace Tests\Model;

use App\Model\Destination;
use App\Model\LocationPoint;
use App\Model\Route;
use Tests\BaseUnitTestCase;

/**
 * Class RouteModelTest
 * @package Tests\Model
 */
class RouteModelTest extends BaseUnitTestCase
{
    public function testDistanceIncreasesWhenDestinationIsAdded()
    {
        $route = new Route(new LocationPoint(1, -4));
        $destination = new Destination(15, new LocationPoint(4, 6), 'test name', null);
        $destination2 = new Destination(7.8, new LocationPoint(4, 6), 'test name2', null);
        $this->assertEquals(0, $route->getTotalDistance());
        $route->addDestination($destination);
        $this->assertEquals(15, $route->getTotalDistance());
        $route->addDestination($destination2);
        $this->assertEquals(22.8, $route->getTotalDistance());
    }

    public function testDistanceIsRemovedWhenRemovedLastDestination()
    {
        $route = new Route(new LocationPoint(1, -4));
        $destination = new Destination(15, new LocationPoint(4, 6), 'test name', null);
        $destination2 = new Destination(7.8, new LocationPoint(4, 6), 'test name2', null);
        $route->addDestination($destination);
        $route->addDestination($destination2);
        $this->assertEquals(22.8, $route->getTotalDistance());
        $route->removeLastDestination();
        $this->assertEquals(15, $route->getTotalDistance());
    }

    public function testHandlesDestinationRemoveOnEmptyDestinationArray()
    {
        $route = new Route(new LocationPoint(1, -4));
        $route->removeLastDestination();
        $this->assertEquals(0, $route->getTotalDistance());
    }
}
