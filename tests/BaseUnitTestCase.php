<?php
declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;

/**
 * Class BaseUnitTestCase
 * @package Tests\DataProcessor
 */
class BaseUnitTestCase extends TestCase
{
    /**
     * @param array @array
     *
     * @return \Generator|[]
     */
    protected function arrayAsGenerator(array $array)
    {
        foreach ($array as $item) {
            yield $item;
        }
    }
}
