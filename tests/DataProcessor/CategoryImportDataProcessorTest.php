<?php
declare(strict_types=1);

namespace Tests\DataProcessor;

use App\DataProcessor\CategoryImportDataProcessor;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;
use Tests\BaseUnitTestCase;

/**
 * Class CategoryImportDataProcessorTest
 *
 * @package Tests\DataProcessor
 */
class CategoryImportDataProcessorTest extends BaseUnitTestCase
{
    /**
     * @var CategoryImportDataProcessor
     */
    protected $categoryDataImportProcessor;

    /**
     * @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityManager;

    /**
     * @var ResultSet|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultSet;

    public function setUp()
    {
        $this->resultSet = $this->createMock(ResultSet::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->categoryDataImportProcessor = new CategoryImportDataProcessor($this->entityManager);
    }

    public function testItemsAreProcessed()
    {
        $data = [
            [
                'id' => 1,
                'cat_name' => 'CategoryName',
            ]
        ];

        $category = new Category();
        $category->setOriginalImportId(1);
        $category->setName('CategoryName');

        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($category);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');
        $this->categoryDataImportProcessor->process($this->resultSet);
    }
}
