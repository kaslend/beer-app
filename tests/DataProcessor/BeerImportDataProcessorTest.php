<?php
declare(strict_types=1);

namespace Tests\DataProcessor;

use App\DataProcessor\BeerImportDataProcessor;
use App\Entity\Beer;
use App\Entity\Brewery;
use App\Entity\Category;
use App\Entity\Style;
use App\Repository\BreweryRepository;
use App\Repository\StyleRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;
use Tests\BaseUnitTestCase;

/**
 * Class BeerImportDataProcesssorTest
 * @package Tests\DataProcessor
 */
class BeerImportDataProcessorTest extends BaseUnitTestCase
{
    /**
     * @var BeerImportDataProcessor
     */
    protected $beerImportDataProcessor;

    /**
     * @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityManager;

    /**
     * @var StyleRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $styleRepository;

    /**
     * @var BreweryRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $breweryRepository;

    /**
     * @var ResultSet|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultSet;

    public function setUp()
    {
        $this->resultSet = $this->createMock(ResultSet::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->styleRepository = $this->createMock(StyleRepository::class);
        $this->breweryRepository = $this->createMock(BreweryRepository::class);

        $this->beerImportDataProcessor = new BeerImportDataProcessor(
            $this->entityManager,
            $this->styleRepository,
            $this->breweryRepository
        );
    }

    public function testItemsAreProcessed()
    {
        $category = new Category();
        $style = new Style();
        $style->setCategory($category);
        $style->setOriginalImportId(1);
        $brewery = new Brewery();
        $brewery->setOriginalImportId(1);


        $data = [
            [
                'id' => 1,
                'name' => 'beerName',
                'style_id' => 1,
                'brewery_id' => 1,
            ]
        ];
        $beer = new Beer();
        $beer->setOriginalImportId(1);
        $beer->setName('beerName');
        $beer->setStyle($style);
        $beer->setBrewery($brewery);
        $beer->setCategory($category);

        $this->styleRepository->expects($this->once())->method('findAllImported')->willReturn([1 => $style]);
        $this->breweryRepository->expects($this->once())->method('findAllImported')->willReturn([1 => $brewery]);

        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($beer);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');
        $this->beerImportDataProcessor->process($this->resultSet);
    }

    public function testItemsAreProcessedWhenStyleIsMissing()
    {
        $brewery = new Brewery();
        $brewery->setOriginalImportId(1);


        $data = [
            [
                'id' => 1,
                'name' => 'beerName',
                'style_id' => 1,
                'brewery_id' => 1,
            ]
        ];
        $beer = new Beer();
        $beer->setOriginalImportId(1);
        $beer->setName('beerName');
        $beer->setBrewery($brewery);

        $this->styleRepository->expects($this->once())->method('findAllImported')->willReturn([]);
        $this->breweryRepository->expects($this->once())->method('findAllImported')->willReturn([1 => $brewery]);

        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($beer);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');
        $this->beerImportDataProcessor->process($this->resultSet);
    }

    public function testItemsAreProcessedWhenBreweryIsMissing()
    {
        $category = new Category();
        $style = new Style();
        $style->setCategory($category);
        $style->setOriginalImportId(1);


        $data = [
            [
                'id' => 1,
                'name' => 'beerName',
                'style_id' => 1,
                'brewery_id' => 1,
            ]
        ];
        $beer = new Beer();
        $beer->setOriginalImportId(1);
        $beer->setName('beerName');
        $beer->setStyle($style);
        $beer->setCategory($category);

        $this->styleRepository->expects($this->once())->method('findAllImported')->willReturn([1 => $style]);
        $this->breweryRepository->expects($this->once())->method('findAllImported')->willReturn([]);

        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($beer);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');
        $this->beerImportDataProcessor->process($this->resultSet);
    }
}
