<?php
declare(strict_types=1);

namespace Tests\DataProcessor;

use App\DataProcessor\BreweriesImportDataProcessor;
use App\Entity\Brewery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;
use Tests\BaseUnitTestCase;

/**
 * Class BreweriesImportDataProcessorTest
 * @package Tests\DataProcessor
 */
class BreweriesImportDataProcessorTest extends BaseUnitTestCase
{
    /**
     * @var BreweriesImportDataProcessor
     */
    protected $breweriesImportDataProcessor;

    /**
     * @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityManager;

    /**
     * @var ResultSet|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultSet;

    public function setUp()
    {
        $this->resultSet = $this->createMock(ResultSet::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->breweriesImportDataProcessor = new BreweriesImportDataProcessor($this->entityManager);

        $locationData = [
            [
                'brewery_id' => 1,
                'latitude' => 41.43920135498000,
                'longitude' => -87.10780334472700,
            ]
        ];

        $locationResultSet = new ResultSet($this->arrayAsGenerator($locationData), []);
        $this->breweriesImportDataProcessor->loadGeolocationData($locationResultSet);
    }

    public function testItemsAreProcessed()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'brewerieName',
                'address1' => 'address',
                'city' => 'city',
                'code' => 123456,
                'country' => 'country',
            ]
        ];

        $brewery = new Brewery();
        $brewery->setOriginalImportId(1);
        $brewery->setName('brewerieName');
        $brewery->setAddress('address');
        $brewery->setCity('city');
        $brewery->setCode(123456);
        $brewery->setCountry('country');
        $brewery->setLong(-87.10780334472700);
        $brewery->setLat(41.43920135498000);

        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($brewery);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');

        $this->breweriesImportDataProcessor->process($this->resultSet);
    }

    public function testItemsAreProcessedWithNoLocationData()
    {
        $data = [
            [
                'id' => 4,
                'name' => 'brewerieName',
                'address1' => 'address',
                'city' => 'city',
                'code' => 123456,
                'country' => 'country',
            ]
        ];

        $brewery = new Brewery();
        $brewery->setOriginalImportId(4);
        $brewery->setName('brewerieName');
        $brewery->setAddress('address');
        $brewery->setCity('city');
        $brewery->setCode(123456);
        $brewery->setCountry('country');

        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($brewery);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');

        $this->breweriesImportDataProcessor->process($this->resultSet);
    }
}
