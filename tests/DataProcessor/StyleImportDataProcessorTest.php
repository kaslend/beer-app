<?php
declare(strict_types=1);

namespace Tests\DataProcessor;

use App\DataProcessor\CategoryImportDataProcessor;
use App\DataProcessor\StyleImportDataProcessor;
use App\Entity\Category;
use App\Entity\Style;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;
use Tests\BaseUnitTestCase;

/**
 * Class StyleImportDataProcessorTest
 * @package Tests\DataProcessor
 */
class StyleImportDataProcessorTest extends BaseUnitTestCase
{
    /**
     * @var CategoryImportDataProcessor
     */
    protected $categoryDataImportProcessor;

    /**
     * @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityManager;

    /**
     * @var CategoryRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $categoryRepository;

    /**
     * @var ResultSet|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $resultSet;

    public function setUp()
    {
        $this->resultSet = $this->createMock(ResultSet::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->categoryRepository = $this->createMock(CategoryRepository::class);
        $this->categoryDataImportProcessor = new StyleImportDataProcessor(
            $this->entityManager,
            $this->categoryRepository
        );
    }

    public function testItemsAreProcessed()
    {
        $category = new Category();
        $category->setOriginalImportId(4);
        $category->setName('CategoryName');

        $categoriesData = [];
        $categoriesData[4] = $category;

        $data = [
            [
                'id' => 1,
                'style_name' => 'style_name',
                'cat_id' => 4,
            ]
        ];

        $style = new Style();
        $style->setName('style_name');
        $style->setOriginalImportId(1);
        $style->setCategory($category);

        $this->categoryRepository->expects($this->once())->method('findAllImported')->willReturn($categoriesData);
        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($style);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');
        $this->categoryDataImportProcessor->process($this->resultSet);
    }


    public function testItemsAreProcessedAndDoesntBreakWhenCategoryIsNotFound()
    {
        $category = new Category();
        $category->setOriginalImportId(6);
        $category->setName('CategoryName');

        $categoriesData = [];
        $categoriesData[4] = $category;

        $data = [
            [
                'id' => 1,
                'style_name' => 'style_name',
                'cat_id' => 6,
            ]
        ];

        $style = new Style();
        $style->setName('style_name');
        $style->setOriginalImportId(1);

        $this->categoryRepository->expects($this->once())->method('findAllImported')->willReturn($categoriesData);
        $this->resultSet->expects($this->once())->method('getRecords')->willReturn($this->arrayAsGenerator($data));
        $this->entityManager->expects($this->once())->method('persist')->with($style);
        $this->entityManager->expects($this->once())->method('flush');
        $this->entityManager->expects($this->once())->method('clear');
        $this->categoryDataImportProcessor->process($this->resultSet);
    }
}
