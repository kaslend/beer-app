<?php
declare(strict_types=1);

namespace Tests\DataProcessor;

use App\DataProvider\BreweryDataProvider;
use App\Entity\Brewery;
use App\Model\LocationPoint;
use App\Repository\BreweryRepository;
use App\Util\DistanceCalculationUtil;
use Tests\BaseUnitTestCase;

/**
 * Class BeerImportDataProcessorTest
 * @package Tests\DataProcessor
 */
class BreweryDataProviderTest extends BaseUnitTestCase
{
    /**
     * @var BreweryRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $breweryRepository;

    /**
     * @var DistanceCalculationUtil|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $distanceCalculationUtil;

    /**
     * @var BreweryDataProvider
     */
    protected $breweryDataProvider;

    public function setUp()
    {
        $this->breweryRepository = $this->createMock(BreweryRepository::class);
        $this->distanceCalculationUtil = $this->createMock(DistanceCalculationUtil::class);

        $this->breweryDataProvider = new BreweryDataProvider(
            $this->breweryRepository,
            $this->distanceCalculationUtil
        );
    }

    public function testReturnsEmptyArrayWhenNoBreweriesFound()
    {
        $this->breweryRepository->expects($this->once())->method('findAllWithLocationData')->willReturn([]);
        $result = $this->breweryDataProvider->loadBreweriesInRange(2000, new LocationPoint(100, -200));
        $this->assertEmpty($result);
    }

    public function testDiscardsBreweryNotInRange()
    {
        $brewery = new Brewery();
        $brewery->setLat(100.0);
        $brewery->setLong(-100.0);
        $brewery2 = new Brewery();
        $brewery2->setLat(200.0);
        $brewery2->setLong(-200.0);
        $breweries = [$brewery, $brewery2];

        $this->breweryRepository->expects($this->once())->method('findAllWithLocationData')->willReturn($breweries);

        $this
            ->distanceCalculationUtil
            ->expects($this->exactly(2))
            ->method('getDistanceBetweenPoints')
            ->willReturnOnConsecutiveCalls(2400, 1400);
        $result = $this->breweryDataProvider->loadBreweriesInRange(2000, new LocationPoint(100, -200));

        $this->assertCount(1, $result);
    }
}
