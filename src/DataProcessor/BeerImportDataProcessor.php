<?php
declare(strict_types=1);

namespace App\DataProcessor;

use App\Entity\Beer;
use App\Entity\Brewery;
use App\Entity\Style;
use App\Repository\BreweryRepository;
use App\Repository\StyleRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;

class BeerImportDataProcessor
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var StyleRepository
     */
    protected $styleRepository;

    /**
     * @var BreweryRepository
     */
    protected $breweryRepository;

    /**
     * @var Style[]
     */
    protected $styles;

    /**
     * @var Brewery[]
     */
    protected $breweries;

    /**
     * BeerImportDataProcessor constructor.
     * @param EntityManagerInterface $entityManager
     * @param StyleRepository $styleRepository
     * @param BreweryRepository $breweryRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        StyleRepository $styleRepository,
        BreweryRepository $breweryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->styleRepository = $styleRepository;
        $this->breweryRepository = $breweryRepository;
    }

    public function process(ResultSet $records)
    {
        $this->initDataForImport();
        $recordCount = 0;
        foreach ($records->getRecords() as $record) {
            $beer = new Beer();
            $beer->setOriginalImportId((int) $record['id']);
            $beer->setName($record['name']);

            if (array_key_exists($record['style_id'], $this->styles)) {
                $beer->setStyle($this->styles[$record['style_id']]);
                $beer->setCategory($this->styles[$record['style_id']]->getCategory());
            }

            if (array_key_exists($record['brewery_id'], $this->breweries)) {
                $beer->setBrewery($this->breweries[$record['brewery_id']]);
            }

            $this->entityManager->persist($beer);

            $recordCount++;
            if ($recordCount >= 300) {
                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->initDataForImport();
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    protected function initDataForImport()
    {
        $this->styles = $this->styleRepository->findAllImported();
        $this->breweries = $this->breweryRepository->findAllImported();
    }
}
