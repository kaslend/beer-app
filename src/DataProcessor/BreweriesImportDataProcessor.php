<?php
declare(strict_types=1);

namespace App\DataProcessor;

use App\Entity\Brewery;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;

class BreweriesImportDataProcessor
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $geolocationData;

    /**
     * CategoryImportDataProcessor constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function process(ResultSet $records)
    {
        foreach ($records->getRecords() as $record) {
            $brewery = new Brewery();
            $brewery->setOriginalImportId((int)$record['id']);
            $brewery->setName($record['name']);
            $brewery->setAddress($record['address1']);
            $brewery->setCity($record['city']);
            $brewery->setCode((int)$record['code']);
            $brewery->setCountry($record['country']);

            if (array_key_exists($record['id'], $this->geolocationData)) {
                $brewery->setLat((float) $this->geolocationData[$record['id']]['latitude']);
                $brewery->setLong((float) $this->geolocationData[$record['id']]['longitude']);
            }

            $this->entityManager->persist($brewery);
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    public function loadGeolocationData(ResultSet $records)
    {
        $this->geolocationData = [];

        foreach ($records as $record) {
            $this->geolocationData[$record['brewery_id']] = $record;
        }
    }
}
