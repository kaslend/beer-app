<?php
declare(strict_types=1);

namespace App\DataProcessor;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;

class CategoryImportDataProcessor
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * CategoryImportDataProcessor constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function process(ResultSet $records)
    {
        foreach ($records->getRecords() as $record) {
            $category = new Category();
            $category->setOriginalImportId((int) $record['id']);
            $category->setName($record['cat_name']);
            $this->entityManager->persist($category);
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
