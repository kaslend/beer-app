<?php
declare(strict_types=1);

namespace App\DataProcessor;

use App\Entity\Style;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\ResultSet;

class StyleImportDataProcessor
{
    protected static $defaultName = 'app:import:categories';

    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * @var CategoryRepository $categoryRepository
     */
    protected $categoryRepository;

    /**
     * StyleImportDataProcessor constructor.
     * @param EntityManagerInterface $entityManager
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(EntityManagerInterface $entityManager, CategoryRepository $categoryRepository)
    {
        $this->entityManager = $entityManager;
        $this->categoryRepository = $categoryRepository;
    }

    public function process(ResultSet $records)
    {
        $categories = $this->categoryRepository->findAllImported();
        foreach ($records->getRecords() as $record) {
            $style = new Style();
            $style->setOriginalImportId((int) $record['id']);
            $style->setName($record['style_name']);

            if (array_key_exists($record['cat_id'], $categories)) {
                $style->setCategory($categories[$record['cat_id']]);
            }

            $this->entityManager->persist($style);
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
