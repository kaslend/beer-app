<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Style;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class StyleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Style::class);
    }

    /**
     * @return Style[]
     */
    public function findAllImported()
    {
        return $this->createQueryBuilder('s', 's.originalImportId')
            ->where('s.originalImportId IS NOT NULL')
            ->addOrderBy('s.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
