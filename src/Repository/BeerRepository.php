<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Beer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class BeerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Beer::class);
    }

    /**
     * @param array $breweries
     * @return mixed
     */
    public function getByBreweries(array $breweries)
    {
        $qb = $this->createQueryBuilder('b');
        return $qb->where($qb->expr()->in('b.brewery', $breweries))->getQuery()->getResult();
    }
}
