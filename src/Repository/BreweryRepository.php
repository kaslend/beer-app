<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Brewery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class BreweryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Brewery::class);
    }

    /**
     * @return Brewery[]
     */
    public function findAllImported()
    {
        return $this->createQueryBuilder('b', 'b.originalImportId')
            ->where('b.originalImportId IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Brewery[]
     */
    public function findAllWithLocationData()
    {
        return $this->createQueryBuilder('b', 'b.originalImportId')
            ->where('b.lat IS NOT NULL')
            ->andWhere('b.long IS NOT NULL')
            ->getQuery()
            ->getResult();
    }
}
