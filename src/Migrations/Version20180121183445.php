<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180121183445 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C1DC311CCC ON category (original_import_id)');
        $this->addSql('ALTER TABLE brewery ADD latitude NUMERIC(18, 14) DEFAULT NULL, ADD longitude NUMERIC(18, 14) DEFAULT NULL, DROP lat, DROP `long`, CHANGE address address VARCHAR(255) DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE state state VARCHAR(255) DEFAULT NULL, CHANGE code code INT DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1A599547DC311CCC ON brewery (original_import_id)');
        $this->addSql('ALTER TABLE beer CHANGE brewery_id brewery_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL, CHANGE style_id style_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_58F666ADDC311CCC ON beer (original_import_id)');
        $this->addSql('ALTER TABLE style CHANGE category_id category_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_33BDB86ADC311CCC ON style (original_import_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_58F666ADDC311CCC ON beer');
        $this->addSql('ALTER TABLE beer CHANGE brewery_id brewery_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL, CHANGE style_id style_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_1A599547DC311CCC ON brewery');
        $this->addSql('ALTER TABLE brewery ADD lat NUMERIC(18, 14) DEFAULT \'NULL\', ADD `long` NUMERIC(18, 14) DEFAULT \'NULL\', DROP latitude, DROP longitude, CHANGE address address VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE city city VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE state state VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE code code INT DEFAULT NULL, CHANGE country country VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX UNIQ_64C19C1DC311CCC ON category');
        $this->addSql('DROP INDEX UNIQ_33BDB86ADC311CCC ON style');
        $this->addSql('ALTER TABLE style CHANGE category_id category_id INT DEFAULT NULL');
    }
}
