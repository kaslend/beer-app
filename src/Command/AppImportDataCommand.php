<?php
declare(strict_types=1);

namespace App\Command;

use App\Csv\DataReader;
use App\DataProcessor\BeerImportDataProcessor;
use App\DataProcessor\BreweriesImportDataProcessor;
use App\DataProcessor\CategoryImportDataProcessor;
use App\DataProcessor\StyleImportDataProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppImportDataCommand extends Command
{
    protected static $defaultName = 'app:import-data';

    /**
     * @var CategoryImportDataProcessor
     */
    protected $categoryImportDataProcessor;

    /**
     * @var StyleImportDataProcessor
     */
    protected $stylesImportDataProcessor;

    /**
     * @var BreweriesImportDataProcessor
     */
    protected $breweriesImportDataProcessor;

    /**
     * @var BeerImportDataProcessor
     */
    protected $beerImportDataProcessor;

    /**
     * @var DataReader
     */
    protected $dataReader;

    public function __construct(
        DataReader $dataReader,
        CategoryImportDataProcessor $categoryImportDataProcessor,
        StyleImportDataProcessor $stylesImportDataProcessor,
        BreweriesImportDataProcessor $breweriesDataProcessor,
        BeerImportDataProcessor $beerImportDataProcessor
    ) {
        parent::__construct();
        $this->categoryImportDataProcessor = $categoryImportDataProcessor;
        $this->stylesImportDataProcessor = $stylesImportDataProcessor;
        $this->breweriesImportDataProcessor = $breweriesDataProcessor;
        $this->beerImportDataProcessor = $beerImportDataProcessor;
        $this->dataReader = $dataReader;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import categories data from csv file.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Importing data.');

        $io->text('Importing categories data.');
        $categoryRecords = $this->dataReader->getDataFromFile('%kernel.root_dir%/../resources/data/categories.csv');
        $this->categoryImportDataProcessor->process($categoryRecords);
        $io->text('Categories import success');

        $io->text('Importing styles data.');
        $styleRecords = $this->dataReader->getDataFromFile('%kernel.root_dir%/../resources/data/styles.csv');
        $this->stylesImportDataProcessor->process($styleRecords);
        $io->text('Styles import success');

        $io->text('Importing breweries data.');
        $geolocationRecords = $this->dataReader->getDataFromFile('%kernel.root_dir%/../resources/data/geocodes.csv');
        $this->breweriesImportDataProcessor->loadGeolocationData($geolocationRecords);
        $breweriesRecords = $this->dataReader->getDataFromFile('%kernel.root_dir%/../resources/data/breweries.csv');
        $this->breweriesImportDataProcessor->process($breweriesRecords);
        $io->text('Breweries import success');

        $io->text('Importing beer data.');
        $beerRecords = $this->dataReader->getDataFromFile('%kernel.root_dir%/../resources/data/beers.csv');
        $this->beerImportDataProcessor->process($beerRecords);
        $io->text('Beer import success.');

        $io->success('Data import success!');
    }
}
