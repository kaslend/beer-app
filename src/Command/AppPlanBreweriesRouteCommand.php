<?php

namespace App\Command;

use App\Model\LocationPoint;
use App\Repository\BeerRepository;
use App\RoutePlanner\BreweriesTripPlanner;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class AppPlanBreweriesRouteCommand extends Command
{
    protected static $defaultName = 'app:plan-breweries-trip';

    /**
     * @var BreweriesTripPlanner
     */
    protected $breweriesTripPlanner;

    /**
     * @var BeerRepository
     */
    protected $beerRepository;

    const DEFAULT_TRAVEL_DISTANCE = 2000;

    /**
     * AppPlanBreweriesRouteCommand constructor.
     * @param BreweriesTripPlanner $breweriesTripPlanner
     * @param BeerRepository $beerRepository
     */
    public function __construct(BreweriesTripPlanner $breweriesTripPlanner, BeerRepository $beerRepository)
    {
        parent::__construct();
        $this->breweriesTripPlanner = $breweriesTripPlanner;
        $this->beerRepository = $beerRepository;
    }


    protected function configure()
    {
        $this
            ->setDescription('Calculate rout to visit breweries.')
            ->addOption('lat', null, InputOption::VALUE_REQUIRED, 'latitude')
            ->addOption('long', null, InputOption::VALUE_REQUIRED, 'longitude')
            ->addOption(
                'distance',
                null,
                InputOption::VALUE_OPTIONAL,
                'Destination to travel.',
                self::DEFAULT_TRAVEL_DISTANCE
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $stopwatch = new Stopwatch();
        $stopwatch->start('programStarted');

        $startLat = $input->getOption('lat');
        $startLong = $input->getOption('long');
        $distance = $input->getOption('distance');

        $planedRoute = $this->breweriesTripPlanner->planRoute($distance, new LocationPoint($startLat, $startLong));

        $routeTable = [];
        $routeTable[] = [
            'HOME',
            0,
            $planedRoute->getStartingLocation()->getLatitude() . ',' .
            $planedRoute->getStartingLocation()->getLongitude()
        ];

        $breweries = [];

        foreach ($planedRoute->getDestinations() as $destination) {
            $tableRow = [];
            $tableRow[] = $destination->getName();
            $tableRow[] = round($destination->getDistance(), 2);
            $tableRow[] =
                $destination->getLocationPoint()->getLatitude() . ',' .
                $destination->getLocationPoint()->getLongitude();
            $routeTable[] = $tableRow;
            if ($destination->getBrewery()) {
                $breweries[] = $destination->getBrewery();
            }
        }

        $io->table(['Brewery name', 'Distance', 'Coordinates'], $routeTable);

        $event = $stopwatch->stop('programStarted');
        $io->writeln('Total distance traveled: ' . round($planedRoute->getTotalDistance(), 2) . 'km');
        $io->writeln('Total breweries visited: ' . (count($planedRoute->getDestinations()) - 1));
        $io->writeln('duration: ' . $event->getDuration() . 'ms');
        $io->writeln('');

        $beers = $this->beerRepository->getByBreweries($breweries);
        $io->writeln('Collected ' . count($beers) . 'Beer types');

        $beerTable = [];
        foreach ($beers as $beer) {
            $beerTable[] = [
                $beer->getId(),
                $beer->getName(),
            ];
        }

        $io->table(['id', 'name'], $beerTable);
        $io->success('Route has been planned successfully');
    }
}
