<?php
declare(strict_types=1);

namespace App\Csv;

use League\Csv\Reader;
use League\Csv\Statement;

class DataReader
{
    /**
     * @param string $filePath
     * @return \League\Csv\ResultSet
     */
    public function getDataFromFile(string $filePath)
    {
        $reader = Reader::createFromPath($filePath);
        $reader->setHeaderOffset(0);
        $records = (new Statement())->process($reader);

        return $records;
    }
}
