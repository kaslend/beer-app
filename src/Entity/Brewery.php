<?php
declare(strict_types=1);

namespace App\Entity;

use App\Model\LocationPoint;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BreweryRepository")
 */
class Brewery
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(type="integer", name="original_import_id", unique=true)
     */
    protected $originalImportId;

    /**
     * @var string
     * @ORM\Column(type="string", name="name")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", name="address", nullable=true)
     */
    protected $address;

    /**
     * @var string
     * @ORM\Column(type="string", name="city", nullable=true)
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(type="string", name="state", nullable=true)
     */
    protected $state;

    /**
     * @var int
     * @ORM\Column(type="integer", name="code", nullable=true)
     */
    protected $code;

    /**
     * @var string
     * @ORM\Column(type="string", name="country", nullable=true)
     */
    protected $country;

    /**
     * @var double
     * @ORM\Column(type="decimal", name="latitude", precision=18, scale=14, nullable=true)
     */
    protected $lat;

    /**
     * @var double
     * @ORM\Column(type="decimal", name="longitude", precision=18, scale=14, nullable=true)
     */
    protected $long;

    public function __toString()
    {
        return (string) $this->id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOriginalImportId(): ?int
    {
        return $this->originalImportId;
    }

    /**
     * @param int $originalImportId
     * @return Brewery
     */
    public function setOriginalImportId(?int $originalImportId): Brewery
    {
        $this->originalImportId = $originalImportId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Brewery
     */
    public function setName(?string $name): Brewery
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Brewery
     */
    public function setAddress(?string $address): Brewery
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Brewery
     */
    public function setCity(?string $city): Brewery
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return Brewery
     */
    public function setState(?string $state): Brewery
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Brewery
     */
    public function setCode(?int $code): Brewery
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Brewery
     */
    public function setCountry(?string $country): Brewery
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     * @return Brewery
     */
    public function setLat(?float $lat): Brewery
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param float $long
     * @return Brewery
     */
    public function setLong(?float $long): Brewery
    {
        $this->long = $long;
        return $this;
    }

    /**
     * @return LocationPoint|null
     */
    public function getLocationPoint(): ?LocationPoint
    {
        if ($this->lat && $this->long) {
            return new LocationPoint((float) $this->lat, (float) $this->long);
        }

        return null;
    }
}
