<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BeerRepository")
 */
class Beer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", name="original_import_id", unique=true)
     */
    private $originalImportId;

    /**
     * @var string
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @var Brewery
     *
     * @ORM\ManyToOne(targetEntity="Brewery")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brewery_id", referencedColumnName="id", nullable=true)
     * })
     */
    protected $brewery;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     * })
     */
    protected $category;

    /**
     * @var Style
     *
     * @ORM\ManyToOne(targetEntity="Style")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="style_id", referencedColumnName="id", nullable=true)
     * })
     */
    protected $style;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOriginalImportId(): ?int
    {
        return $this->originalImportId;
    }

    /**
     * @param int $originalImportId
     * @return Beer
     */
    public function setOriginalImportId(?int $originalImportId): Beer
    {
        $this->originalImportId = $originalImportId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Beer
     */
    public function setName(?string $name): Beer
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Brewery
     */
    public function getBrewery(): ?Brewery
    {
        return $this->brewery;
    }

    /**
     * @param Brewery $brewery
     * @return Beer
     */
    public function setBrewery(?Brewery $brewery): Beer
    {
        $this->brewery = $brewery;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Beer
     */
    public function setCategory(?Category $category): Beer
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Style
     */
    public function getStyle(): ?Style
    {
        return $this->style;
    }

    /**
     * @param Style $style
     * @return Beer
     */
    public function setStyle(?Style $style): Beer
    {
        $this->style = $style;
        return $this;
    }
}
