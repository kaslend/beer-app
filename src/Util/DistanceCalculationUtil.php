<?php
declare(strict_types=1);

namespace App\Util;

use App\Model\LocationPoint;

class DistanceCalculationUtil
{
    const EARTH_RADIUS = 6371;
    /**
     * @param LocationPoint $locationPointA
     * @param LocationPoint $locationPointB
     * @return float|int
     */
    public function getDistanceBetweenPoints(LocationPoint $locationPointA, LocationPoint $locationPointB)
    {

        $dLat = deg2rad($locationPointB->getLatitude() - $locationPointA->getLatitude());
        $dLon = deg2rad($locationPointB->getLongitude() - $locationPointA->getLongitude());

        $a =
            sin($dLat / 2) *
            sin($dLat / 2) +
            cos(deg2rad($locationPointA->getLatitude())) *
            cos(deg2rad($locationPointB->getLatitude())) *
            sin($dLon / 2) * sin($dLon / 2);

        $c = 2 * asin(sqrt($a));
        $d = self::EARTH_RADIUS * $c;

        return $d;
    }
}
