<?php
declare(strict_types=1);

namespace App\RoutePlanner\Util;

use App\Entity\Brewery;
use App\Model\Destination;
use App\Model\LocationPoint;
use App\Util\DistanceCalculationUtil;

class BreweriesTripPlannerUtil
{
    /**
     * @var DistanceCalculationUtil
     */
    protected $distanceCalculationUtil;

    /**
     * BreweriesTripPlannerUtil constructor.
     * @param DistanceCalculationUtil $distanceCalculationUtil
     */
    public function __construct(DistanceCalculationUtil $distanceCalculationUtil)
    {
        $this->distanceCalculationUtil = $distanceCalculationUtil;
    }

    /**
     * @param LocationPoint $locationPoint
     * @param array $breweries
     *
     * @return Destination
     */
    public function getNearestDestination(LocationPoint $locationPoint, array $breweries): Destination
    {
        /**
         * @var Brewery
         */
        $nearestBrewery = null;
        $nearestDistance = null;

        foreach ($breweries as $brewery) {
            $distance = $this->distanceCalculationUtil->getDistanceBetweenPoints(
                $locationPoint,
                $brewery->getLocationPoint()
            );

            if (!$nearestDistance || $nearestDistance > $distance) {
                $nearestDistance = $distance;
                $nearestBrewery = $brewery;
            }
        }

        return new Destination(
            $nearestDistance,
            $nearestBrewery->getLocationPoint(),
            $nearestBrewery->getName(),
            $nearestBrewery
        );
    }
}
