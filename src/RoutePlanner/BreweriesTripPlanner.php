<?php
declare(strict_types=1);

namespace App\RoutePlanner;

use App\DataProvider\BreweryDataProvider;
use App\Model\Destination;
use App\Model\LocationPoint;
use App\Model\Route;
use App\RoutePlanner\Util\BreweriesTripPlannerUtil;
use App\Util\DistanceCalculationUtil;

/**
 * Class BreweriesTripPlanner
 * @package App\RoutePlanner
 */
class BreweriesTripPlanner
{
    /**
     * @var DistanceCalculationUtil
     */
    protected $distanceCalculationUtil;

    /**
     * @var BreweryDataProvider
     */
    protected $breweryDataProvider;

    /**
     * @var BreweriesTripPlannerUtil
     */
    protected $breweriesTripPlannerUtil;

    /**
     * BreweriesTripPlanner constructor.
     * @param DistanceCalculationUtil $distanceCalculationUtil
     * @param BreweryDataProvider $breweryDataProvider
     * @param BreweriesTripPlannerUtil $breweriesTripPlannerUtil
     */
    public function __construct(
        DistanceCalculationUtil $distanceCalculationUtil,
        BreweryDataProvider $breweryDataProvider,
        BreweriesTripPlannerUtil $breweriesTripPlannerUtil
    ) {
        $this->distanceCalculationUtil = $distanceCalculationUtil;
        $this->breweryDataProvider = $breweryDataProvider;
        $this->breweriesTripPlannerUtil = $breweriesTripPlannerUtil;
    }

    public function planRoute(int $maxDistance, LocationPoint $startingStarting)
    {
        $breweriesInRange = $this->breweryDataProvider->loadBreweriesInRange($maxDistance, $startingStarting);
        $route = new Route($startingStarting);

        $currentLocation = clone $route->getStartingLocation();

        while ($route->getTotalDistance() < $maxDistance && !empty($breweriesInRange)) {
            $destination = $this->breweriesTripPlannerUtil->getNearestDestination(
                $currentLocation,
                $breweriesInRange
            );

            unset($breweriesInRange[$destination->getBrewery()->getId()]);

            $distanceToHome =
                $this->distanceCalculationUtil->getDistanceBetweenPoints(
                    $currentLocation,
                    $route->getStartingLocation()
                );

            $futureDistance = $route->getTotalDistance() + $destination->getDistance() + $distanceToHome;

            if ($futureDistance >= $maxDistance) {
                if (($route->getTotalDistance() + $distanceToHome) > $maxDistance) {
                    $route->removeLastDestination();
                }

                $route->addDestination(new Destination($distanceToHome, $route->getStartingLocation(), 'HOME', null));
                break;
            }

            $route->addDestination($destination);

            unset($currentLocation);
            $currentLocation = clone $destination->getBrewery()->getLocationPoint();
        }

        return $route;
    }
}
