<?php
declare(strict_types=1);

namespace App\DataProvider;

use App\Entity\Brewery;
use App\Model\LocationPoint;
use App\Repository\BreweryRepository;
use App\Util\DistanceCalculationUtil;

class BreweryDataProvider
{
    /**
     * @var BreweryRepository
     */
    protected $breweryRepository;

    /**
     * @var DistanceCalculationUtil
     */
    protected $distanceCalculationUtil;

    /**
     * BreweryDataProvider constructor.
     * @param BreweryRepository $breweryRepository
     * @param DistanceCalculationUtil $distanceCalculationUtil
     */
    public function __construct(BreweryRepository $breweryRepository, DistanceCalculationUtil $distanceCalculationUtil)
    {
        $this->breweryRepository = $breweryRepository;
        $this->distanceCalculationUtil = $distanceCalculationUtil;
    }

    /**
     * @param int $range
     * @param LocationPoint $locationPoint
     * @return array
     */
    public function loadBreweriesInRange(int $range, LocationPoint $locationPoint)
    {
        $breweries = $this->breweryRepository->findAllWithLocationData();
        $breweriesInRange = [];

        /**
         * @var Brewery $brewery
         */
        foreach ($breweries as $key => $brewery) {
            if ($brewery->getLocationPoint()) {
                $distance = $this->distanceCalculationUtil->getDistanceBetweenPoints(
                    $locationPoint,
                    $brewery->getLocationPoint()
                );
                if ($distance <= $range) {
                    $breweriesInRange[$brewery->getId()] = $brewery;
                }
            }
        }

        return $breweriesInRange;
    }
}
