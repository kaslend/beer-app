<?php
declare(strict_types=1);

namespace App\Model;

/**
 * Class Route
 * @package App\Model
 */
class Route
{
    /**
     * @var float
     */
    protected $totalDistance;
    /**
     * @var Destination[]
     */
    protected $destinations;

    /**
     * @var LocationPoint
     */
    protected $startingLocation;

    /**
     * Route constructor.
     * @param LocationPoint $startingLocation
     */
    public function __construct(LocationPoint $startingLocation)
    {
        $this->startingLocation = $startingLocation;
        $this->totalDistance = 0;
        $this->destinations = [];
    }


    /**
     * @return float
     */
    public function getTotalDistance(): float
    {
        return $this->totalDistance;
    }

    public function addDestination(Destination $destination): Route
    {
        $this->totalDistance += $destination->getDistance();
        $this->destinations[] = $destination;

        return $this;
    }

    public function removeLastDestination(): Route
    {
        $lastDestination = end($this->destinations);
        if ($lastDestination) {
            $this->totalDistance -= $lastDestination->getDistance();
            unset($this->destinations[key($lastDestination)]);
        }

        return $this;
    }

    /**
     * @return LocationPoint
     */
    public function getStartingLocation(): LocationPoint
    {
        return $this->startingLocation;
    }

    /**
     * @return Destination[]
     */
    public function getDestinations(): array
    {
        return $this->destinations;
    }
}
