<?php
declare(strict_types=1);

namespace App\Model;

/**
 * Class LocationPoint
 * @package App\Model
 */
class LocationPoint
{
    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * LocationPoint constructor.
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }


    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }
}
