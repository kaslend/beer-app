<?php
declare(strict_types=1);

namespace App\Model;

use App\Entity\Brewery;

/**
 * Class Destination
 * @package App\Model
 */
class Destination
{
    /**
     * @var float
     */
    protected $distance;

    /**
     * @var LocationPoint
     */
    protected $locationPoint;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Brewery
     */
    protected $brewery;

    /**
     * Destination constructor.
     * @param float $distance
     * @param LocationPoint $locationPoint
     * @param string $name
     * @param Brewery $brewery
     */
    public function __construct(float $distance, LocationPoint $locationPoint, string $name, ?Brewery $brewery)
    {
        $this->distance = $distance;
        $this->locationPoint = $locationPoint;
        $this->name = $name;
        $this->brewery = $brewery;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return LocationPoint
     */
    public function getLocationPoint(): LocationPoint
    {
        return $this->locationPoint;
    }

    /**
     * @return Brewery
     */
    public function getBrewery(): ?Brewery
    {
        return $this->brewery;
    }
}
